< Logo >

# < Nom Projet >

< pipeline status >

## 🧐 A propos

< Description >

## 🏁 Pour démarrer

Les instructions ci-dessous vont vous permettre de démarrer le projet.

```shell
npm run start

#Si c'est la première fois, installez les dépendances
npm run install:php
```

## 🎈 Usage

< Documentation du projet >


## 🚀 Déploiement

Lors de la création d'une nouvelle version un CHANGELOG est automatiquement créé.
Vous pouvez retrouver tous les changements sur le fichier [CHANGELOG](CHANGELOG.md)

## 💡 Contribuer

Le projet est entierement "dockerisé". Plusieurs raccourcis npm ont été créés pour simplifier les commandes.

### Démarrer le conteneur

```shell
# Commande raccourci
npm run start

# Commande directe
docker-compose up -d
```

### Entrer dans le conteneur

```shell
# Commande raccourci
npm run exec

# Commande directe
docker-compose exec api bash
```

### Stopper le conteneur

```shell
# Commande raccourci
npm run stop

# Commande directe
docker-compose stop
```

### Supprimer le conteneur

```shell
# Commande raccourci
npm run rm

# Commande directe
docker-compose rm -s -v -f
```

### Les commits

Les commits doivent être normalisé afin de pouvoir créer le changelog automatiquement. Pour cela vous pouvez utiliser :

```shell
npm run commit
```

### Installer les dépendances

```shell
# Commande raccourci
npm run install:php

# Commande directe
docker-compose exec api composer install
```

### Commande Symfony

Pour exécuter une commande symfony rapidement vous pouvez utiliser la commande suivante 

```shell
# Commande raccourci
npm run sy:co <command>

# Commande directe
docker-compose exec api bin/console <command>
```
