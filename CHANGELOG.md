# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.3 (2021-11-01)


### 🏗 Chore

* version 0.0.2 ([f6c53f5](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/f6c53f54d2a10ccf92f96434c40d4739d7969805))


### ✨ Features

* ajout du dossier config/packages/prod ([ce00453](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/ce00453ac7de2b56dd5517b8bb1b46936538ceec))
* correction + ajout README ([1064c35](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/1064c35a6399cf4a202ec55c1130f9c85ed338e6))
* init du skeleton symfony API ([b7a2ed2](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/b7a2ed2153ed0aac8a4862e4d9d0fb6856cb3a92))
* Supprime doctrine ([e06fbbe](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/e06fbbe6d055c1e813e565a315575a65fe235987))


### 🐛 Bug Fixes

* fixe le nom du package dans le composer.json ([4323dcb](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/4323dcbcbeb75ed4e7acd071ee176911c6abbfa6))
* supprime la mise à jour du composer lock dans le changelog ([25627c1](https://gitlab.com/tonyschmitt/php-bundle/skeleton-symfony-api/commit/25627c14c63148e482fc26b3deba3790477b177a))
