# Skeleton Bundle

## 🧐 A propos

Skeleton pour créer un projet symfony en clean architecture.

## 🏁 Pour démarrer
### Prérequis

Outils nécessaires :
- docker

### Créer le projet


Pour créer votre bundle il vous suffit de lancer la commande suivante via docker :

```shell
docker run --rm --interactive --tty --volume $PWD:/app composer create-project tonyschmitt/skeleton-symfony-api <nomProjet>
```

## 🚀 Déploiement

A chaque nouvelle version, le [CHANGELOG](CHANGELOD.md) est mis à jour automatiquement.
